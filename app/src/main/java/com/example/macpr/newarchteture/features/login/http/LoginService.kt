package com.example.macpr.newarchteture.features.login.http

import com.example.macpr.newarchteture.features.login.domain.UserVO
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Work on 29/10/2017.
 */
interface LoginService {
    @GET("kpi3f")
    fun getUser(): Observable<UserVO>
}