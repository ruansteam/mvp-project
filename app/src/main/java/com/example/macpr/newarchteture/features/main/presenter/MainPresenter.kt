package com.example.macpr.newarchteture.features.main.presenter

import android.content.Context
import android.os.Bundle
import android.support.v4.widget.SearchViewCompat.setQuery
import android.support.v7.widget.SearchView
import com.example.macpr.newarchteture.common.presenter.Presenter
import com.example.macpr.newarchteture.common.presenter.ServicePresenter
import com.example.macpr.newarchteture.common.util.RxSearch
import com.example.macpr.newarchteture.features.main.activity.view.MainViewModel
import com.example.macpr.newarchteture.features.main.adapter.MainAdapter
import com.example.macpr.newarchteture.features.main.domain.Mural
import com.example.macpr.newarchteture.features.main.httpService.MainService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import io.reactivex.subjects.BehaviorSubject


/**
 * Created by Ruan Correa on 01/11/17.
 */

class MainPresenter(view: MainViewModel? = null,
                    context: Context? = null,
                    bundle: Bundle? = null) : Presenter<MainViewModel>(view, context, bundle) {

    lateinit var list: MutableList<Mural>
    lateinit var filtered: MutableList<Mural>
    var adapter: MainAdapter? = null

    override fun onCreate() {
        getMural()
    }

    fun onBackPressed() {
        adapter?.updateList(list)
        adapter?.notifyDataSetChanged()
    }

    private fun getMural() {
        Observable.just(MainService.getPlaces())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            list = it
                            filtered = it
                            adapter = MainAdapter(context!!, filtered)
                            adapter?.setHasStableIds(true)
                            adapter?.let { view?.setAdapter(it) }
                            adapter?.notifyDataSetChanged()
                        },
                        onError = {

                        }
                )
    }

    fun setQueryListener(searchView: SearchView?) {
        searchView?.let {
            RxSearch.fromSearchView(searchView)
                    .debounce(1000, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { query ->
                        filtered = list.filter { item -> item.title.toLowerCase().contains(query) || item.subtitle.toLowerCase().contains(query) }.toMutableList()
                        adapter?.updateList(filtered)
                        adapter?.notifyDataSetChanged()
                    }
        }
    }
}