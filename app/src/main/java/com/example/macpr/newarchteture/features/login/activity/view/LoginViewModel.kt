package com.example.macpr.newarchteture.features.login.activity.view

import com.example.macpr.newarchteture.common.activity.view.BaseViewModel
import com.example.macpr.newarchteture.features.login.domain.UserVO

/**
 * Created by Ruan Correa on 01/11/17.
 */

open interface LoginViewModel : BaseViewModel {
    fun showLoginFields()
    fun showOrDismissLoader()
    fun showMain(it: UserVO)
    fun showErrorLogin()
    fun forceDismissLoader(fieldsOpened: Boolean)
    fun dismisError()
}