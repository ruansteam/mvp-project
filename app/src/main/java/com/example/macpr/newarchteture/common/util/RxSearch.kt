package com.example.macpr.newarchteture.common.util

import android.support.v7.widget.SearchView
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * Created by Joao on 17/11/2017.
 */
object RxSearch {

    fun fromSearchView(searchView: SearchView): Observable<String> {
        val subject = BehaviorSubject.create<String>()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {
                subject.onComplete()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (!newText.isEmpty()) {
                    subject.onNext(newText)
                }
                return true
            }
        })

        searchView.setOnClickListener {
            //Clear query
            searchView.setQuery("", false)
            //Collapse the action view
            searchView.onActionViewCollapsed()
            subject.onNext("")
        }

        return subject
    }
}