package com.example.macpr.newarchteture.common.presenter

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.os.Bundle
import com.example.macpr.newarchteture.common.activity.view.BaseViewModel
import com.example.macpr.newarchteture.common.application.MVPApplication
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by macpr on 27/10/17.
 */
abstract class ServicePresenter<out T, S: BaseViewModel>(clazz: Class<T>,
                                                         view: S? = null,
                                                         context: Context? = null,
                                                         bundle: Bundle? = null) : Presenter<S>() {

    init {
        this.view = view
        this.context = context
        this.bundle = bundle
    }

    val retrofitService: T by lazy {
        Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.myjson.com/bins/")
                .build().create<T>(clazz)
    }

    val localService: T by lazy {
        clazz.newInstance()
    }
}

abstract class Presenter<S: BaseViewModel?>(var view: S? = null,
                         var context: Context? = null,
                         var bundle: Bundle? = null) : LifecycleObserver {

    init {
        onCreate()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    abstract fun onCreate()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestroy() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause() {
    }
}
