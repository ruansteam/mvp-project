package com.example.macpr.newarchteture.common.activity

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import android.content.res.Configuration
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.example.macpr.newarchteture.common.activity.view.BaseViewModel
import com.example.macpr.newarchteture.common.presenter.Presenter

/**
 * Created by Ruan Correa on 27/10/17.
 */
abstract class BaseActivity<T: Presenter<S>, S: BaseViewModel>(var presenterClass: Class<T>): AppCompatActivity(), LifecycleOwner {

    lateinit var presenter: T
    private val lifecycleRegistry: LifecycleRegistry by lazy { LifecycleRegistry(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = presenterClass.newInstance()
        presenter.view = getViewModel()
        presenter.bundle = intent.extras
        presenter.context = applicationContext

        registryPresenter(presenter)
    }

    abstract fun getViewModel(): S?

    override fun getLifecycle(): Lifecycle = lifecycleRegistry

    open fun registryPresenter(presenter: T) {
        lifecycle.addObserver(presenter)
    }
}