package com.example.macpr.newarchteture.features.main.domain

import java.sql.Timestamp

/**
 * Created by Work on 13/11/2017.
 */
data class Mural(var id: Long,
                 var title: String,
                 var subtitle: String,
                 var description: String,
                 var image: Int)