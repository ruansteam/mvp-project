package com.example.macpr.newarchteture.common.application

import android.app.Application
import retrofit2.Retrofit



/**
 * Created by Work on 29/10/2017.
 */
class MVPApplication: Application() {
    companion object {
        private var appInstance : MVPApplication? = null

        fun getInstance() : MVPApplication {
            if (appInstance == null) {
                appInstance = MVPApplication()
            }

            return appInstance!!
        }

    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this
    }
}