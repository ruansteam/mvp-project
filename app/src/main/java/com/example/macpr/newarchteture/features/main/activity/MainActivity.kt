package com.example.macpr.newarchteture.features.main.activity

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.example.macpr.newarchteture.R
import com.example.macpr.newarchteture.common.activity.BaseActivity
import com.example.macpr.newarchteture.features.main.activity.view.MainViewModel
import com.example.macpr.newarchteture.features.main.adapter.MainAdapter
import com.example.macpr.newarchteture.features.main.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.widget.SearchView


class MainActivity : BaseActivity<MainPresenter, MainViewModel>(MainPresenter::class.java), MainViewModel {

    private var mSearchAction: MenuItem? = null
    private var isSearchOpened = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.setHasFixedSize(true)

        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        val myActionMenuItem = menu?.findItem(R.id.action_search)
        val searchView = myActionMenuItem?.actionView as? SearchView
//        searchView?.setOnQueryTextListener(presenter.searchQueryListener())
        presenter.setQueryListener(searchView)

        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    override fun getViewModel(): MainViewModel {
        return this
    }

    override fun setAdapter(adapter: MainAdapter) {
        recycler.adapter = adapter
    }
}
