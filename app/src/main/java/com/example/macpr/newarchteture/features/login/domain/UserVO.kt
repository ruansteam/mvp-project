package com.example.macpr.newarchteture.features.login.domain

/**
 * Created by Work on 29/10/2017.
 */
open class UserVO(val name: String,
                  val age: Int,
                  val birthday: String,
                  val success: Boolean)