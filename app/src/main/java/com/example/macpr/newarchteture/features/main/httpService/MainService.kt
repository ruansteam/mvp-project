package com.example.macpr.newarchteture.features.main.httpService

import com.example.macpr.newarchteture.R
import com.example.macpr.newarchteture.features.main.domain.Mural

/**
 * Created by Work on 15/11/2017.
 */
class MainService {
    companion object {
        fun getPlaces(): MutableList<Mural> {
            var items = mutableListOf<Mural>()
            var coliseum = Mural(1,
                    "Coliseum (Italy)",
                    "The Roman Colosseum or Coliseum, originally known as the Flavian Amphitheatre, was commisioned in AD 72 by Emperor Vespasian. It was completed by his son, Titus, in 80, with later improvements by Domitian.",
                    "The Colosseum is located just east of the Roman Forum and was built to a practical design, with its 80 arched entrances allowing easy access to 55,000 spectators, who were seated according to rank. The Coliseum is huge, an ellipse 188m long and 156 wide. Originally 240 masts were attached to stone corbels on the 4th level.\nJust outside the Coliseum is the Arch of Constantine (Arco di Costantino), a 25m high monument built in AD315 to mark the victory of Constantine over Maxentius at Pons Milvius.\nVespesian ordered the Colosseum to be build on the site of Nero's palace, the Domus Aurea, to dissociate himself from the hated tyrant. \n" +
                            "His aim was to gain popularity by staging deadly combats of gladiators and wild animal fights for public viewing. Massacre was on a huge scale: at inaugural games in AD 80, over 9,000 wild animals were killed.",
                    R.drawable.coliseum)
            var christh = Mural(2,
                    "Christ the Redeemer",
                    "The statue weighs 635 metric tons (625 long, 700 short tons), and is located at the peak of the 700-metre (2,300 ft) Corcovado mountain in the Tijuca Forest National Park overlooking the city of Rio de Janeiro. A symbol of Christianity across the world, the statue has also become a cultural icon of both Rio de Janeiro and Brazil, and is listed as one of the New Seven Wonders of the World.[3] It is made of reinforced concrete and soapstone.",
                    "Christ the Redeemer, Portuguese Cristo Redentor, colossal statue of Jesus Christ at the summit of Mount Corcovado, Rio de Janeiro, southeastern Brazil. It was completed in 1931 and stands 98 feet (30 metres) tall, its horizontally outstretched arms spanning 92 feet (28 metres). The statue, made of reinforced concrete clad in a mosaic of thousands of triangular soapstone tiles, sits on a square stone pedestal base about 26 feet (8 metres) high, which itself is situated on a deck atop the mountain’s summit. The statue is the largest Art Deco-style sculpture in the world and is one of Rio de Janeiro’s most recognizable landmarks.\nIn the 1850s the Vincentian priest Pedro Maria Boss suggested placing a Christian monument on Mount Corcovado to honour Isabel, princess regent of Brazil and the daughter of Emperor Pedro II, although the project was never approved. In 1921 the Roman Catholic archdiocese of Rio de Janeiro proposed that a statue of Christ be built on the 2,310-foot (704-metre) summit, which, because of its commanding height, would make it visible from anywhere in Rio. Citizens petitioned Pres. Epitácio Pessoa to allow the construction of the statue on Mount Corcovado.",
                    R.drawable.christ_the_redeemer)
            var machuPicchu = Mural(3,
                    "Machu Picchu",
                    "Most modern archaeologists and historians agree that Machu Picchu was built by the Inca Pachacutec, the greatest statesman of Tahuantinsuyo, who ruled from 1438 to 1471. Archaeologists assume that the construction of the citadel would date from the fifteenth century approximately chronological date given by the carbon-14 or radiocarbon.",
                    "The construction of Machu Picchu began when the Inca´s territory started to grow. According to archaeologists, in this area was fought the last battle that defined victory over the Chancas, covering prestigious victory and gave power to the Inca Pachacutec.\nInca Pachacutec was the first to emerge beyond the valley of Cusco after his epic victory over the Chancas. He conducted the Tahuantinsuyo expansion and recognized it as the \"constructor\" of Cusco. This was one of his greatest works.\nThe origin of Machu Picchu is attributed with some certainty to Pachacutec, embattled president, which was characterized by territorial conquests, and the development of religion and spirituality. From today there is archaeological studies supportting the theory gods and a challenge to the ruler to built skills.\n",
                    R.drawable.machupicchu)
            var greatWall = Mural(4,
                    "Great Wall of China",
                    "The Great Wall of China is one of the greatest sights in the world — the longest wall in the world, an awe-inspiring feat of ancient defensive architecture. Its winding path over rugged country and steep mountains takes in some great scenery.",
                    "The Great Wall, one of the greatest wonders of the world, was listed as a World Heritage by UNESCO in 1987. Just like a gigantic dragon, it winds up and down across deserts, grasslands, mountains and plateaus, stretching approximately 21,196 kilometers from east to west of China.\nWith a history of about 2,700 years, some of the Great Wall sections are now in ruins or have disappeared. However, the Great Wall of China is still one of the most appealing attractions all around the world owing to its architectural grandeur and historical significance.",
                    R.drawable.great_wall_china)
            var pyramid = Mural(5,
                    "Great Pyramid of Giza",
                    "The Great Pyramid of Giza is a defining symbol of Egypt and the last of the ancient Seven Wonders of the World. It is located on the Giza plateau near the modern city of Cairo and was built over a twenty-year period during the reign of the king Khufu (2589-2566 BCE, also known as Cheops) of the 4th Dynasty.",
                    "The pyramid was first excavated using modern techniques and scientific analysis in 1880 CE by Sir William Matthew Flinders Petrie (1853-1942 CE), the British archaeologist who set the standard for archaeological operations in Egypt generally and at Giza specifically.\nAlthough many theories persist as to the purpose of the pyramid, the most widely accepted understanding is that it was constructed as a tomb for the king. Exactly how it was built, however, still puzzles people in the modern day. The theory of ramps running around the outside of the structure to move the blocks into place has been largely discredited. So-called \"fringe\" or \"New Age\" theories abound, in an effort to explain the advanced technology required for the structure, citing extra-terrestrials and their imagined frequent visits to Egypt in antiquity. These theories continue to be advanced in spite of the increasing body of evidence substantiating that the pyramid was built by the ancient Egyptians using technological means which, most likely, were so common to them that they felt no need to record them. Still, the intricacy of the interior passages, shafts, and chambers (The King's Chamber, Queen's Chamber, and Grand Gallery) as well as the nearby Osiris Shaft, coupled with the mystery of how the pyramid was built at all and its orientation to cardinal points, encourages the persistence of these fringe theories.  ",
                    R.drawable.great_pyramid_of_giza)
            var petra = Mural(6,
                    "Petra",
                    "The \"Lost City\" still has secrets to reveal: Thousands of years ago, the now-abandoned city of Petra was thriving.",
                    "Carved directly into vibrant red, white, pink, and sandstone cliff faces, the prehistoric Jordanian city of Petra was \"lost\" to the Western world for hundreds of years.\nLocated amid rugged desert canyons and mountains in what is now the southwestern corner of the Hashemite Kingdom of Jordan, Petra was once a thriving trading center and the capital of the Nabataean empire between 400 B.C. and A.D. 106.\nThe city sat empty and in near ruin for centuries. Only in the early 1800s did a European traveler disguise himself in Bedouin costume and infiltrate the mysterious locale.",
                    R.drawable.ruins_petra)
            var tajMahal = Mural(7,
                    "Taj Mahal",
                    "The Taj Mahal of Agra is one of the Seven Wonders of the World, for reasons more than just looking magnificent. It's the history of Taj Mahal that adds a soul to its magnificence: a soul that is filled with love, loss, remorse, and love again. ",
                    "The Taj Mahal of Agra is one of the Seven Wonders of the World, for reasons more than just looking magnificent. It's the history of Taj Mahal that adds a soul to its magnificence: a soul that is filled with love, loss, remorse, and love again. Because if it was not for love, the world would have been robbed of a fine example upon which people base their relationships. An example of how deeply a man loved his wife, that even after she remained but a memory, he made sure that this memory would never fade away. This man was the Mughal Emperor Shah Jahan, who was head-over-heels in love with Mumtaz Mahal, his dear wife. She was a Muslim Persian princess (her name Arjumand Banu Begum before marriage) and he was the son of the Mughal Emperor Jehangir and grandson of Akbar the Great. It was at the age of 14 that he met Mumtaz and fell in love with her. Five years later in the year 1612, they got married.\nMumtaz Mahal, an inseparable companion of Shah Jahan, died in 1631, while giving birth to their 14th child. It was in the memory of his beloved wife that Shah Jahan built a magnificent monument as a tribute to her, which we today know as the \"Taj Mahal\". The construction of Taj Mahal started in the year 1631. Masons, stonecutters, inlayers, carvers, painters, calligraphers, dome-builders and other artisans were requisitioned from the whole of the empire and also from Central Asia and Iran, and it took approximately 22 years to build what we see today. An epitome of love, it made use of the services of 22,000 laborers and 1,000 elephants. The monument was built entirely out of white marble, which was brought in from all over India and central Asia. After an expenditure of approximately 32 million rupees, Taj Mahal was finally completed in the year 1653.",
                    R.drawable.taj_mahal)

            items.add(coliseum)
            items.add(christh)
            items.add(machuPicchu)
            items.add(greatWall)
            items.add(pyramid)
            items.add(petra)
            items.add(tajMahal)
            return items
        }
    }
}