package com.example.macpr.newarchteture.features.main.activity.view

import com.example.macpr.newarchteture.common.activity.view.BaseViewModel
import com.example.macpr.newarchteture.features.main.adapter.MainAdapter

/**
 * Created by Ruan Correa on 01/11/17.
 */
interface MainViewModel: BaseViewModel {
    fun setAdapter(adapter: MainAdapter)
}