package com.example.macpr.newarchteture.features.main.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.macpr.newarchteture.R
import com.example.macpr.newarchteture.features.main.domain.Mural
import kotlinx.android.synthetic.main.mural_item.view.*
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.toast

/**
 * Created by Work on 13/11/2017.
 */
class MainAdapter(var context: Context, var list: MutableList<Mural>): RecyclerView.Adapter<MainAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item = list[position]
        holder?.let { holder ->
            with(holder.itemView) {
                cardView.setOnClickListener(onClickItem(item))
                Glide.with(this).load(item.image).into(imgItem)
                tvItem.text = item.title
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MainAdapter.ViewHolder = ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.mural_item, parent, false))

    override fun getItemCount(): Int = list.count()

    override fun getItemId(position: Int): Long = list[position].id

    private fun onClickItem(item: Mural): View.OnClickListener? {
        return View.OnClickListener {
            context.toast(item.title)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    fun updateList(filtered: MutableList<Mural>) {
        list = filtered
    }

}