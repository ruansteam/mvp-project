package com.example.macpr.newarchteture.features.simple.login.presenter

import android.content.Context
import android.os.Bundle
import com.example.macpr.newarchteture.features.login.activity.view.LoginViewModel
import com.example.macpr.newarchteture.features.login.http.LoginService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Joao on 17/11/2017.
 */
internal class LoginPresenter(val view: LoginViewModel, val context: Context, val bundle: Bundle?) {
    private var loginFieldsShowed: Boolean = false
    private var fieldsOpened: Boolean = false

    private val retrofitService by lazy { Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.myjson.com/bins/")
            .build().create<LoginService>(LoginService::class.java) }

    fun onCreate() {
    }

    fun onResume() {
        view.forceDismissLoader(fieldsOpened)
    }

    fun onClickLogin(user: String? = null, pass: String? = null) {
        if (!loginFieldsShowed) {
            view.showLoginFields()
            loginFieldsShowed = true
            return
        }

        if (user == "ruan" && pass == "ruan") {
            callLogin()
            fieldsOpened = true
        } else {
            callError()
        }
    }

    private fun callError() {
        view.showErrorLogin()
        Observable.just("")
                .delay(3000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = {
                    view.dismisError()
                })
    }

    private fun callLogin() {
        view.showOrDismissLoader()
        retrofitService.getUser()
                .delay(5000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            view.showMain(it)
//                            throw Exception()
                        },
                        onError = {
                            view.showOrDismissLoader()
                            callError()
                        }
                )
    }
}