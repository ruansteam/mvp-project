package com.example.macpr.newarchteture.features.login.activity

import android.content.res.Configuration
import android.os.Bundle
import android.support.animation.DynamicAnimation
import android.support.animation.SpringAnimation
import android.support.animation.SpringForce
import android.support.animation.SpringForce.*
import android.support.transition.TransitionManager
import android.view.View
import com.example.macpr.newarchteture.R
import com.example.macpr.newarchteture.common.activity.BaseActivity
import com.example.macpr.newarchteture.features.login.domain.UserVO
import com.example.macpr.newarchteture.features.login.presenter.LoginPresenter
import com.example.macpr.newarchteture.features.main.activity.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fields_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import com.example.macpr.newarchteture.common.util.MorphAnimation
import com.example.macpr.newarchteture.features.login.activity.view.LoginViewModel


class LoginActivity : BaseActivity<LoginPresenter, LoginViewModel>(LoginPresenter::class.java), LoginViewModel {

    private lateinit var morphAnimationLoader: MorphAnimation
    private lateinit var morphAnimationError: MorphAnimation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupClicks()
        setupAnimations()
    }

    private fun setupAnimations() {
        morphAnimationLoader = MorphAnimation(container, rootView, loaderViews)
        morphAnimationError = MorphAnimation(container, rootView, errorViews)
    }

    private fun setupClicks() {
        btnLogin.setOnClickListener { presenter.onClickLogin(etUser.text?.toString(), etPassword.text?.toString()) }
    }

    override fun showLoginFields() {
        fieldsView.visibility = View.VISIBLE
        TransitionManager.beginDelayedTransition(rootView)

        var force = SpringForce()
        force.finalPosition = fieldsView.y
        force.dampingRatio = DAMPING_RATIO_MEDIUM_BOUNCY
        force.stiffness = STIFFNESS_LOW

        var anim = SpringAnimation(fieldsView, DynamicAnimation.TRANSLATION_Y)
        anim.spring = force
        anim.setStartVelocity(2000f)
        anim.start()
    }

    override fun getViewModel(): LoginViewModel? {
        return this
    }

    override fun showMain(it: UserVO) {
        toast(it.name)
        startActivity<MainActivity>()
    }

    override fun showOrDismissLoader() {
        if (!morphAnimationLoader.isPressed) {
            fieldsView.visibility = View.GONE
            btnLogin.visibility = View.GONE
            loaderViews.visibility = View.VISIBLE
            morphAnimationLoader.morphIntoForm()
        } else {
            fieldsView.visibility = View.VISIBLE
            btnLogin.visibility = View.VISIBLE
            loaderViews.visibility = View.GONE
            morphAnimationLoader.morphIntoButton()
        }
    }

    override fun forceDismissLoader(fieldsOpened: Boolean) {
        if (fieldsOpened) {
            fieldsView.visibility = View.VISIBLE
        }
        btnLogin.visibility = View.VISIBLE
        loaderViews.visibility = View.GONE
        container.visibility = View.VISIBLE
        morphAnimationLoader.morphIntoButton()
    }

    override fun showErrorLogin() {
        if (!morphAnimationError.isPressed) {
            fieldsView.visibility = View.GONE
            btnLogin.visibility = View.GONE
            errorViews.visibility = View.VISIBLE
            morphAnimationError.morphIntoForm()
        }
    }

    override fun dismisError() {
        if (morphAnimationError.isPressed) {
            fieldsView.visibility = View.VISIBLE
            btnLogin.visibility = View.VISIBLE
            errorViews.visibility = View.GONE
            container.visibility = View.VISIBLE
            morphAnimationError.morphIntoButton()
        }
    }
}