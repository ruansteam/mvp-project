package com.example.macpr.newarchteture.features.login.presenter

import android.content.Context
import android.os.Bundle
import com.example.macpr.newarchteture.common.presenter.ServicePresenter
import com.example.macpr.newarchteture.features.login.activity.view.LoginViewModel
import com.example.macpr.newarchteture.features.login.http.LoginService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by macpr on 27/10/17.
 */
class LoginPresenter(view: LoginViewModel? = null, context: Context? = null, bundle: Bundle? = null) : ServicePresenter<LoginService, LoginViewModel>(LoginService::class.java, view, context, bundle) {
    private var loginFieldsShowed: Boolean = false
    private var fieldsOpened: Boolean = false

    override fun onCreate() {}

    override fun onResume() {
        super.onResume()
        view?.forceDismissLoader(fieldsOpened)
    }

    fun onClickLogin(user: String? = null, pass: String? = null) {
        if (!loginFieldsShowed) {
            view?.showLoginFields()
            loginFieldsShowed = true
            return
        }

        if (user == "ruan" && pass == "ruan") {
            callLogin()
            fieldsOpened = true
        } else {
            callError()
        }
    }

    private fun callError() {
        view?.showErrorLogin()
        Observable.just("")
                .delay(3000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = {
                    view?.dismisError()
                })
    }

    private fun callLogin() {
        view?.showOrDismissLoader()
        retrofitService.getUser()
                .delay(5000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            view?.showMain(it)
//                            throw Exception()
                        },
                        onError = {
                            view?.showOrDismissLoader()
                            callError()
                        }
                )
    }
}